package ar.edu.unju.fi.trabajoDeJPA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ar.edu.unju.fi.Service.Implementacion.LibroServiceImp;
import ar.edu.unju.fi.entity.Libro;

@SpringBootApplication
public class TrabajoDeJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrabajoDeJpaApplication.class, args);


		LibroServiceImp libroServiceImp = new LibroServiceImp();
		Libro libro1 = new Libro("El Principito", "Alan Poe", "jkl999", 50);
       Libro  libro2 = new Libro("Cien anios de Soledad", "Gabriel Garcia Marquez", "col-123", 10);


 
	   libroServiceImp.crearLibro(libro1);
	   libroServiceImp.crearLibro(libro2);

	}

}
