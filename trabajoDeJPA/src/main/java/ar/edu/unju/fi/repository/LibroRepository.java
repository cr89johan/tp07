package ar.edu.unju.fi.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.entity.Libro;

@Repository
public interface LibroRepository extends CrudRepository<Libro, Long> {

    public List<Libro> getgetByAutor();

    public List<Libro> getByIsbn();

    public List<Libro> getByTitulo();

}
