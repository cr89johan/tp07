package ar.edu.unju.fi.Service.Implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.Service.IMiembroService;
import ar.edu.unju.fi.entity.Miembro;
import ar.edu.unju.fi.repository.MiembroRepository;

@Service
public class MiembroServiceIpm implements IMiembroService {

    @Autowired
    MiembroRepository miembroRepository;

    @Override
    public void crearMiembro(Miembro miembro) {
        miembroRepository.save(miembro);

    }

    @Override
    public void editarMiembro(Miembro miembro) {
        miembroRepository.save(miembro);

    }

    @Override
    public void eliminarMiembro(Miembro miembro) {
        miembroRepository.delete(miembro);
    }

}
