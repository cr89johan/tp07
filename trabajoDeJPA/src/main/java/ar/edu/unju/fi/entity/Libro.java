package ar.edu.unju.fi.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Libro {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column
    private Long id;
	@Column(name="Titulo")
    private String titulo;
    @Column(name="Autor")
    private String autor;
    @Column(name="ISBN")
    private String isbn;
    @Column(name="Cantidad")
    private int cantidadDisponible;
    
    
    
	public Libro() {
	}
	
	public Libro( String titulo, String autor, String isbn, int cantidadDisponible) {
		super();
		this.titulo = titulo;
		this.autor = autor;
		this.isbn = isbn;
		this.cantidadDisponible = cantidadDisponible;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public int getCantidadDisponible() {
		return cantidadDisponible;
	}
	public void setCantidadDisponible(int cantidadDisponible) {
		this.cantidadDisponible = cantidadDisponible;
	}

	@Override
	public String toString() {
		return "Libro [id=" + id + ", titulo=" + titulo + ", autor=" + autor + ", isbn=" + isbn
				+ ", cantidadDisponible=" + cantidadDisponible + "]";
	}

    
}
