package ar.edu.unju.fi.Service.Implementacion;

import org.springframework.stereotype.Service;

import ar.edu.unju.fi.Service.IPrestamoService;
import ar.edu.unju.fi.entity.Prestamo;
import ar.edu.unju.fi.repository.PrestamoRepository;

@Service
public class PrestamoServiceImp implements IPrestamoService {

    PrestamoRepository prestamoRepository;

    @Override
    public void crearPrestamo(Prestamo prestamo) {
        prestamoRepository.save(prestamo);
    }

    @Override
    public void editarPrestamo(Prestamo prestamo) {
        prestamoRepository.save(prestamo);
    }

    @Override
    public void eliminarPrestamo(Prestamo prestamo) {
        prestamoRepository.delete(prestamo);
    }

}
