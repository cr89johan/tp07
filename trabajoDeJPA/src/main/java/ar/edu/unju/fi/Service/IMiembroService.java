package ar.edu.unju.fi.Service;

import ar.edu.unju.fi.entity.Miembro;

public interface IMiembroService {

    public void crearMiembro(Miembro miembro);

    public void editarMiembro(Miembro miembro);

    public void eliminarMiembro(Miembro miembro);
}
