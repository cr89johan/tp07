package ar.edu.unju.fi.Service;

import ar.edu.unju.fi.entity.Prestamo;

public interface IPrestamoService {
    
    public void crearPrestamo(Prestamo prestamo);

    public void editarPrestamo(Prestamo prestamo);

    public void eliminarPrestamo(Prestamo prestamo);
}
