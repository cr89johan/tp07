package ar.edu.unju.fi.Service.Implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.Service.ILibroService;
import ar.edu.unju.fi.entity.Libro;
import ar.edu.unju.fi.repository.LibroRepository;

@Service
public class LibroServiceImp implements ILibroService {

    @Autowired
    private LibroRepository libroRepository;

    @Override
    public void crearLibro(Libro libro) {
        libroRepository.save(libro);
    }

    @Override
    public void editarLibro(Libro libro) {
        libroRepository.save(libro);
    }

    @Override
    public void eliminarLibro(Libro libro) {
        libroRepository.delete(libro);
    }

    @Override
    public List<Libro> getByAutor(String autor) {
        return libroRepository.getgetByAutor();
    }

    @Override
    public List<Libro> getByIsbn(String isbn) {
        return libroRepository.getByIsbn();
    }

    @Override
    public List<Libro> getByTitulo(String titulo) {
        return libroRepository.getByTitulo();
    }

}
