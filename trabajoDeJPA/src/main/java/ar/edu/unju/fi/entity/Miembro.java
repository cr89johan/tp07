package ar.edu.unju.fi.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Miembro {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "Nombre")
	private String nombre;
	@Column(name = "Numero de Miembro")
	private String numeroMiembro;
	@Column(name = "Email")
	private String correoElectronico;
	@Column(name = "Teléfono")
	private String numeroTelefono;

	public Miembro() {
	}

	public Miembro(String nombre, String numeroMiembro, String correoElectronico, String numeroTelefono) {
		super();
		this.nombre = nombre;
		this.numeroMiembro = numeroMiembro;
		this.correoElectronico = correoElectronico;
		this.numeroTelefono = numeroTelefono;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumeroMiembro() {
		return numeroMiembro;
	}

	public void setNumeroMiembro(String numeroMiembro) {
		this.numeroMiembro = numeroMiembro;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	@Override
	public String toString() {
		return "Miembro [id=" + id + ", nombre=" + nombre + ", numeroMiembro=" + numeroMiembro + ", correoElectronico="
				+ correoElectronico + ", numeroTelefono=" + numeroTelefono + "]";
	}

}
