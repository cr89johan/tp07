package ar.edu.unju.fi.entity;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class Prestamo {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="Id")
    private Long id;

    @ManyToOne
    @Column(name="Libro")
    private Libro libro;

    @ManyToOne
    @Column(name="Usuario")
    private Miembro miembro;

    @Column(name="Fecha de préstamo")
    private Date fechaPrestamo;
    
    @Column(name="Fecha de Devolución")
    private Date fechaDevolucion;

	public Prestamo() {
	}

	public Prestamo(Long id, Libro libro, Miembro miembro, Date fechaPrestamo, Date fechaDevolucion) {
		super();
		this.id = id;
		this.libro = libro;
		this.miembro = miembro;
		this.fechaPrestamo = fechaPrestamo;
		this.fechaDevolucion = fechaDevolucion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Libro getLibro() {
		return libro;
	}

	public void setLibro(Libro libro) {
		this.libro = libro;
	}

	public Miembro getMiembro() {
		return miembro;
	}

	public void setMiembro(Miembro miembro) {
		this.miembro = miembro;
	}

	public Date getFechaPrestamo() {
		return fechaPrestamo;
	}

	public void setFechaPrestamo(Date fechaPrestamo) {
		this.fechaPrestamo = fechaPrestamo;
	}

	public Date getFechaDevolucion() {
		return fechaDevolucion;
	}

	public void setFechaDevolucion(Date fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

	@Override
	public String toString() {
		return "Prestamo [id=" + id + ", libro=" + libro + ", miembro=" + miembro + ", fechaPrestamo=" + fechaPrestamo
				+ ", fechaDevolucion=" + fechaDevolucion + "]";
	}
    
    
}
