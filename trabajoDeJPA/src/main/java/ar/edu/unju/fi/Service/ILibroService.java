package ar.edu.unju.fi.Service;

import java.util.List;

import ar.edu.unju.fi.entity.Libro;

public interface ILibroService {

    public void crearLibro(Libro libro);

    public void editarLibro(Libro libro);

    public void eliminarLibro(Libro libro);

    public List<Libro> getByAutor(String autor);

    public List<Libro> getByIsbn(String isbn);

    public List<Libro> getByTitulo(String titulo);
}
