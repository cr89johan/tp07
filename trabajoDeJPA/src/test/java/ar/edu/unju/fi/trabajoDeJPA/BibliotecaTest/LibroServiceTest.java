package ar.edu.unju.fi.trabajoDeJPA.BibliotecaTest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.unju.fi.Service.Implementacion.LibroServiceImp;
import ar.edu.unju.fi.entity.Libro;
import ar.edu.unju.fi.entity.Miembro;

@SpringBootTest
public class LibroServiceTest {

   static LibroServiceImp libroServiceImp;
   
    static Libro libro1;
    static Libro libro2;
    static Miembro user1;
    static Miembro user2;

    @Test
    @BeforeEach
    @DisplayName("Inicializar variables")
    void setup() {         
        libroServiceImp = new LibroServiceImp(); 
        libro1 = new Libro("El Principito", "Alan Poe", "jkl999", 50);
        libro2 = new Libro("Cien anios de Soledad", "Gabriel Garcia Marquez", "col-123", 10);
        user1 = new Miembro("JUAN CORTEZ", "0018", "jcor@gmail.com", "5676543456");
        user2 = new Miembro("Rocio Lopez", "0020", "rolo@gmail.com", "9348765457");
    }
    
    @Test
    void crearLibroTest(){
        libroServiceImp.crearLibro(libro1);

    }

   
}
