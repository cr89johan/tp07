TRABAJO PRACTICO 07

Sistema de Gestión de Biblioteca
Desarrolla una aplicación de gestión de biblioteca utilizando Java y JPA (Java Persistence API). El sistema deberá
permitir a los bibliotecarios llevar un registro de los libros en la biblioteca, los préstamos realizados a los miembros y la
información básica de los miembros.

Requisitos Funcionales

Gestión de Libros:
Crear, editar y eliminar registros de libros.
Cada libro debe tener id, título, autor, ISBN y cantidad disponible.

Gestión de Miembros:
Crear, editar y eliminar registros de miembros de la biblioteca.
Cada miembro debe tener id, nombre, número de miembro, correo electrónico y número de teléfono.

Búsqueda y Consultas:
Permitir la búsqueda de libros por título, autor o ISBN.
Validación de las funcionalidades
Todas las funcionalidades solicitadas deben realizarse utilizando pruebas unitarias

Implementar la capa de servicios

POR JOHAN ROSERO.

UNJU FI.